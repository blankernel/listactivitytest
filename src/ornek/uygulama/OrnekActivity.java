package ornek.uygulama;

import android.app.ListActivity;
import android.content.Context;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.view.View;
import android.view.ViewGroup;

public class OrnekActivity extends ListActivity {
	@Override
	public void onCreate(Bundle b){
		super.onCreate(b);
		
		// Liste öğelerini ayarla
		String[] items = {
			"BLANKERNEL",
			"Android Programlama",
			"Örnek Liste"
		};
		
		// Yeni bir Adapter nesnesi tanımla
		ListAdapter adapter = new ListAdapter(this,items);
		
		// Adapter nesnesini ListActivity içinde bulunan
		// ListView nesnesine tanımla
		getListView().setAdapter(adapter);
	}

	// boş, String kabul eden bir Adapter sınıfı oluştur
	private static class ListAdapter extends ArrayAdapter<String> {
		
		ListAdapter(Context ctx, String... objects){
			super(ctx, android.R.layout.simple_list_item_1,android.R.id.text1,objects);
		}
		
		// Tanımlı olan getView metodunun üzerine yaz
		@Override
		public View getView(int pos, View view, ViewGroup parent){
			
			// Eğer view henüz tanımlanmamış ise
			// super metodunu kullan
			if(view == null){
				view = super.getView(pos, view, parent);
			}
			
			// Metin kutusunu getir
			TextView tv = view.findViewById(android.R.id.text1);
			
			// Metin rengini çiftse kırmızı, tekse yeşil yap
			// Renk formatı: ARGB (0xAARRGGBB)
			tv.setTextColor((pos + 1) % 2 == 0 ? 0xFFAA0000 : 0xFF00CC00);
			
			// view nesnesini geri döndürüyoruz
			return view;
		}
	}
}
